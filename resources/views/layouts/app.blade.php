<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @if (Auth::check()) <meta name="user_id" content="{{ Auth::user()->id }}" />@else <meta name="user_id" content="0" /> @endif
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>Inventory System</title>
    
    <!-- Styles -->
    <link rel="stylesheet" href="./css/app.css">
    <link href="./css/sb-admin-2.css" rel="stylesheet">
    <link href="./css/style.css" rel="stylesheet">
</head>
<body>
    <div id="app">
        <div class="splashpage">
            <div>
                <img src="./images/logo.png" class="img-fluid"/>
            </div>
        </div>
        <main class="">
            @yield('content')
        </main>
    </div>
    
    <script src="/js/app.js"></script>
    <script src="/js/sb-admin-2.js"></script>
    <script src="/js/jquery.easing.js"></script>
    <script>
        $('.splashpage').animate({opacity:0}, 2000, ()=>{
            $('.splashpage').css({display:'none'});
        });
    </script>
</body>
</html>
