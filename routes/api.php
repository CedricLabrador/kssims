<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::apiResources([
    'user' => 'App\Http\Controllers\API\UserController',
    'shelf' => 'App\Http\Controllers\API\ShelfController',
    'item' => 'App\Http\Controllers\API\ItemController',
    'withdraws' => 'App\Http\Controllers\API\WithdrawController'
    ]);

Route::get('allBins', [App\Http\Controllers\API\ShelfController::class, 'allBins']);
Route::get('findQR', [App\Http\Controllers\API\ShelfController::class, 'searchQR']);
Route::get('findShelf', [App\Http\Controllers\API\ShelfController::class, 'search']);
Route::get('findBins', [App\Http\Controllers\API\ShelfController::class, 'searchBins']);

Route::get('allItems', [App\Http\Controllers\API\ItemController::class, 'allItems']);
Route::get('findItem', [App\Http\Controllers\API\ItemController::class, 'search']);
Route::put('updateQty/{item}', [App\Http\Controllers\API\ItemController::class, 'updateQty']);
Route::put('withdraw/{item}', [App\Http\Controllers\API\ItemController::class, 'withdraw']);
Route::put('approveRequest/{item}', [App\Http\Controllers\API\ItemController::class, 'approveRequest']);
Route::put('disapproveRequest/{item}', [App\Http\Controllers\API\ItemController::class, 'disapproveRequest']);

Route::get('allUsers', [App\Http\Controllers\API\UserController::class, 'allUsers']);
Route::get('findUser', [App\Http\Controllers\API\UserController::class, 'search']);