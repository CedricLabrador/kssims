require('./bootstrap');

import Vue from 'vue'
import { Form, HasError, AlertError } from 'vform'
import VueRouter from 'vue-router'
import VueQrcodeReader from "vue-qrcode-reader"
import VueProgressBar from 'vue-progressbar'
import Swal from 'sweetalert2'
import VueQRCodeComponent from 'vue-qr-generator'
import JsonCSV from 'vue-json-csv'
import JsonExcel from "vue-json-excel";

Vue.use(VueRouter)
Vue.use(VueQrcodeReader);

window.Form = Form;
window.Swal = Swal;

Vue.component(AlertError.name, AlertError)
Vue.component(HasError.name, HasError)
Vue.component('qr-code', VueQRCodeComponent)
Vue.component('pagination', require('laravel-vue-pagination'));
Vue.component('downloadCsv', JsonCSV);
Vue.component("downloadExcel", JsonExcel);

Vue.prototype.$userId = document.querySelector("meta[name='user_id']").getAttribute('content');

// PROGRESS BAR
const options = {
    color: '#bffaf3',
    failedColor: '#874b4b',
    thickness: '5px',
    autoRevert: true,
    location: 'top',
    inverse: false
}

Vue.use(VueProgressBar, options)


// ROUTES
const routes = [
    { path: '/', component: require('./components/Dashboard.vue').default },
    { path: '/dashboard', component: require('./components/Dashboard.vue').default },
    // { path: '/logs', component: require('./components/Logs.vue').default },
    { path: '/users', component: require('./components/Users.vue').default },
    { path: '/deposit', component: require('./components/Items.vue').default },
    { path: '/settings', component: require('./components/Settings.vue').default },
    { path: '/bins', component: require('./components/Shelves.vue').default },
    { path: '/withdraw', component: require('./components/Withdraw.vue').default },
    { path: '/transactions', component: require('./components/Transactions.vue').default },
    { path: '/stocks', component: require('./components/Stocks.vue').default },
    { path: '/*', component: require('./components/NotFound.vue').default },
]

// ROUTER
const router = new VueRouter({
    mode: 'history',
    routes,
})


// TOAST
const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
})

window.Toast = Toast;

window.Fire = new Vue();


// APP
const app = new Vue({
    router, 
    data() {
        return {
            search: '',
            user_id: this.$userId,
            shelves: {},
            items: {},
            users: {}
        }
    },
    created() {
        // this.loadData();
        // console.log(this.user_id);
    },
    methods: {
        searchAll: _.debounce(()=>{
            Fire.$emit('searching');
        },1000),
        // loadData: function(){
        //     console.log('Parent Data Loaded');
        //     axios.get("/api/allBins").then(({data}) => (this.shelves = data));
        //     axios.get("/api/allUsers").then(({data}) => (this.users = data));
        //     axios.get("/api/allItems").then(({data}) => (this.items = data));
        // }
    }
}).$mount('#app')
