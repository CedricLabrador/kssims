<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class StockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('items')->insert([
            'bin_id' => '23',
            'name' => Str::random(10),
            'type' => Hash::make('password'),
            'quantity' => rand(1, 100),
            'size' => Str::random(10),
            'type' => Str::random(10)
        ]);
    }
}
