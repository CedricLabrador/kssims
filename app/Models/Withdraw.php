<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Withdraw extends Model
{
    use HasFactory;

    protected $fillable = [
        'shelf_name',
        'shelf_location',
        'item_name',
        'item_size',
        'item_description',
        'item_type',
        'quantity',
        'purpose',
        'main_code',
        'sub_code',
        'item_id',
        'remarks',
        'user_id'
    ];

    public function item()
    {
        return $this->belongsTo(Item::class); 
    }

    public function shelf()
    {
        return $this->belongsTo(Shelf::class); 
    }
}
