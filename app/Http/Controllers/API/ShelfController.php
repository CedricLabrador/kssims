<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Shelf;
use Illuminate\Support\Str;

class ShelfController extends Controller
{

    public function __construct()
    {
        $this->middleware('api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Shelf::with('items')->orderBy('name', 'ASC')->paginate(10);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => ['required', 'string', 'max:255', 'unique:shelves'],
            'location' => ['required', 'string', 'max:255']
        ]);


        return Shelf::create([
            'name' => $request['name'],
            'location' => $request['location'],
            'qrcode' => Str::random(32)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Shelf::with('items')->findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $shelf = Shelf::findOrFail($id);
        
        $this->validate($request,[
            'name' => ['required', 'string', 'max:255', 'unique:shelves,name,'.$shelf->id],
            'location' => ['required', 'string', 'max:255']
           
        ]);

        $shelf->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $shelf = Shelf::findOrFail($id);

        $shelf->delete();
    }

    public function search()
    {
        if ($search = \Request::get('q')) {
            $shelves = Shelf::with('items')->where(function($query) use ($search) {
                $query->where('name', 'LIKE', "%$search%")
                ->orWhere('location', 'LIKE', "%$search%");
            })->paginate(10);
        } else {
            return Shelf::with('items')->latest()->paginate(10);
        }

        return $shelves;
    }

    public function searchQR()
    {
        if ($search = \Request::get('q')) {
            $shelves =Shelf::with('items')->whereHas('items')
            ->where(function($query) use ($search) {
                $query->where('qrcode', 'LIKE', "%$search%");
            })->latest()->get();
        } else {
            return ["msg" => "no result"];
        }

        return $shelves;
    }

    public function searchBins()
    {
        if ($search = \Request::get('q')) {
            $shelves =Shelf::with('items')->whereHas('items')
            ->where(function($query) use ($search) {
                $query->where('name', 'LIKE', "%$search%")
                ->orWhere('location', 'LIKE', "%$search%");
            })->latest()->get();
        } else {
            // return Shelf::with('items')->latest()->paginate(2);
            return ["msg" => "no result"];
        }

        return $shelves;
    }

    public function allBins()
    {
        return Shelf::latest()->get();
    }
}
