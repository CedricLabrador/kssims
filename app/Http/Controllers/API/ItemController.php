<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Item;
use App\Models\Withdraw;
use Illuminate\SUpport\Facades\Auth;

class ItemController extends Controller
{

    public function __construct()
    {
        // $this->middleware('api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Item::with('shelf')->latest()->paginate(15);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'shelf_id' => ['required', 'integer'],
            'name' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string', 'max:255'],
            'quantity' => ['required', 'integer'],
            'size' => ['required', 'string', 'max:255'],
            'type' => ['required', 'string', 'max:255'],
        ]);


        return Item::create([
            'shelf_id' => $request['shelf_id'],
            'name' => $request['name'],
            'description' => $request['description'],
            'quantity' => $request['quantity'],
            'size' => $request['size'],
            'type' => $request['type'],
            'shelf_name' => $request['shelf_name'],
            'shelf_location' => $request['shelf_location'],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Item::findOrFail($id);
        
        $this->validate($request,[
            'shelf_id' => ['required', 'integer'],
            'name' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string', 'max:255'],
            'quantity' => ['required', 'integer'],
            'size' => ['required', 'string', 'max:255'],
            'type' => ['required', 'string', 'max:255'],
        ]);

        $item->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Item::findOrFail($id);

        $item->delete();
    }

    public function updateQty(Request $request, $id)
    {
        $item = Item::findOrFail($id);

        $this->validate($request,[
            'quantity' => ['required', 'integer', 'min:1']
        ]);

        $currentQty = $item->quantity;
        $newQty = $currentQty + $request->quantity;
        $item->quantity = $newQty;
        $item->save();
    }
    public function withdraw(Request $request, $id)
    {

        $item = Item::findOrFail($id);

        $this->validate($request,[
            'quantity' => ['required', 'integer', 'min:1'],
            'purpose' => ['required', 'string', 'max:255'],
            'main_code' => ['required', 'string', 'max:255'],
            'sub_code' => ['required', 'string', 'max:255']
        ]);

        Withdraw::create([
            'shelf_name' => $request['shelf_name'],
            'shelf_location' => $request['shelf_location'],
            'item_name' => $request['item_name'],
            'item_description' => $request['item_description'],
            'item_size' => $request['item_size'],
            'item_type' => $request['item_type'],
            'quantity' => $request['quantity'],
            'purpose' => $request['purpose'],
            'main_code' => $request['main_code'],
            'sub_code' => $request['sub_code'],
            'item_id' => $id,
            'user_id' => $request['userId'],
            'remarks' => 'pending'

        ]);

        // $currentQty = $item->quantity;
        // $newQty = $currentQty - $request->quantity;
        // $item->quantity = $newQty;
        // $item->save();
    }

    public function search()
    {
        if ($search = \Request::get('q')) {
            $items = Item::with('shelf')->whereHas('shelf')
            ->where(function($query) use ($search) {
                $query->where('name', 'LIKE', "%$search%")
                ->orWhere('description', 'LIKE', "%$search%")
                ->orWhere('size', 'LIKE', "%$search%")  
                ->orWhere('type', 'LIKE', "%$search%");
            })->paginate(10);
        } else {
            return Item::with('shelf')->latest()->paginate(15);
        }

        return $items;
    }

    public function allItems()
    {
        return Item::with('shelf')->latest()->get();
    }

    public function approveRequest(Request $request, $id)
    {

        // return $request;
        $withdraw = Withdraw::findOrFail($id);

        $withdraw->remarks = 'approved';
        $withdraw->save();

        $item = Item::findOrFail($request['item_id']);

        $currentQty = $item->quantity;
        $newQty = $currentQty - $request->quantity;
        $item->quantity = $newQty;
        $item->save();

        return $request;
    }

    public function disapproveRequest(Request $request, $id)
    {

        // return $request;
        $withdraw = Withdraw::findOrFail($id);

        $withdraw->remarks = 'disapproved';
        $withdraw->save();

        // $item = Item::findOrFail($request['item_id']);

        // $currentQty = $item->quantity;
        // $newQty = $currentQty - $request->quantity;
        // $item->quantity = $newQty;
        // $item->save();

        return $request;
    }
}
