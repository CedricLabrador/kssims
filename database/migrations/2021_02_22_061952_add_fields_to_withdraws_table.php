<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToWithdrawsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('withdraws', function (Blueprint $table) {
            $table->string('shelf_name')->nullable();
            $table->string('shelf_location')->nullable();
            $table->string('item_name')->nullable();
            $table->string('item_description')->nullable();
            $table->string('item_type')->nullable();
            $table->string('item_size')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('withdraws', function (Blueprint $table) {
            $table->dropColumn('shelf_name');
            $table->dropColumn('shelf_location');
            $table->dropColumn('item_name');
            $table->dropColumn('item_description');
            $table->dropColumn('item_size');
            $table->dropColumn('item_type');
        });
    }
}
