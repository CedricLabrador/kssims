<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    protected $fillable = [
        'shelf_id',
        'shelf_name',
        'shelf_location',
        'name',
        'description',
        'quantity',
        'size',
        'type',

    ];

    public function shelf()
    {
        return $this->belongsTo(Shelf::class); 
    }

    public function withdraws()
    {
        return $this->hasMany(Withdraw::class); 
    }
}
