@extends('layouts.app')

@section('content')
<div class="container">
   
    <div class="row d-flex justify-content-center align-items-center" style="height: 100vh">
        
        <div class="col-md-8">
            <div class="card ">
                <div class="card-header text-center h4">{{ __('Login') }}</div>
                
                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                            <div class="form-group row">
                                <div class="col-lg-3 col-sm-12"></div>
                                <div class="col-lg-6 col-sm-12">
                                    <label for="email" class="text-left" style="text-align: left!important">Email Address</label>
                                </div>
                                <div class="col-lg-3 col-sm-12"></div>
                                <div class="col-lg-3 col-sm-12"></div>
                                <div class="col-lg-6 col-sm-12">
                                    <input type="email" class="form-control w-100 @error('email') is-invalid @enderror" name="email" id="email"  value="{{ old('email') }}" required autocomplete="email" autofocus>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-lg-3 col-sm-12"></div>
                                {{-- <label for="email" class="text-left" style="text-align: left!important">Email Address</label>
                                <center>
                                    <input type="email" class="form-control w-50 @error('email') is-invalid @enderror" name="email" id="email"  value="{{ old('email') }}" required autocomplete="email" autofocus>
                                </center>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror --}}
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-3 col-sm-12"></div>
                                <div class="col-lg-6 col-sm-12">
                                    <label for="password" class="text-left" style="text-align: left!important">Password</label>
                                </div>
                                <div class="col-lg-3 col-sm-12"></div>
                                <div class="col-lg-3 col-sm-12"></div>
                                <div class="col-lg-6 col-sm-12">
                                    <input type="password" class="form-control w-100 @error('password') is-invalid @enderror" name="password" id="password" required autocomplete="current-password">
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-lg-3 col-sm-12"></div>
{{--                                 
                                <center>
                                    <input type="password" class="form-control w-100 @error('password') is-invalid @enderror" name="password" id="password" required autocomplete="current-password">
                                </center>
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div> --}}
                        {{-- <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label">{{ __('E-Mail Address') }}</label>
                            
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div> --}}
                        
                        {{-- <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                            
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div> --}}
                        
                        {{-- <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    
                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div> --}}
                        <div class="col-12 mt-4">
                            <center>
                                <button type="submit" class="btn btn-danger btn-block w-50">Login</button>
                            </center>
                        </div>
                        
                        {{-- <div class="form-group row mb-0">
                            <div class="">
                                <button type="submit" class="btn btn-danger w-50">
                                    {{ __('Login') }}
                                </button>
                            </div>
                        </div> --}}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
