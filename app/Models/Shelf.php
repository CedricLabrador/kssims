<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shelf extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'qrcode',
        'location'
    ];

    public function items()
    {
        return $this->hasMany(Item::class); 
    }

    public function withdraws()
    {
        return $this->hasMany(Withdraw::class); 
    }
}   
